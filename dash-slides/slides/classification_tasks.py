###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Classification Tasks"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

#fig = read_plotly_plot('Input_data')

user_content = html.Div(
    [
        dbc.Card(
            dbc.CardBody(
                [
                    html.H5("Classification based on Points Of Interest (POI)", className="card-title", style=dict(color='green')),
                    html.P([
                        html.Strong("First Question: "),
                        "Can we replicate the measurement being performed by the MicroVu system"
                    ]),
                    html.Ul(html.Li("Reusing measurment locations defined for the MicroVu measuring task")),
                ]
            )
        ),
        html.Br(),
        html.Br(),
        dbc.Card(
            dbc.CardBody(
                [
                    html.H5("Classification based on overall contour", className="card-title", style=dict(color='blue')),
                    html.P([
                        html.Strong("Second Question: "),
                        "Can we go one step further?"
                    ]),
                    html.Ul(html.Li("Consideration of 1,000 data points spread over the whole contour")),
                ]
            )
        )
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

