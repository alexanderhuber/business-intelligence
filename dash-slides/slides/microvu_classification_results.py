###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Classification Based On Points Of Interest (POI)"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center', color='green')),
        html.Hr()
    ])

microvu_reference_classification = read_plotly_plot("microvu_results_per_feature")
value_comparison = read_plotly_plot("POI_classification_deviation")
fig = read_plotly_plot('comparison_microvu_target_values')



classification_outcome_against_logs = read_plotly_plot("poi_classification_against_target_log")
classification_outcome_against_drawing = read_plotly_plot("poi_classification_against_target_drawing")
classification_outcome_against_model = read_plotly_plot("poi_classification_against_target_interpolation")

classification_match_against_logs = read_plotly_plot("classification_match_against_target_log")
classification_match_against_drawing = read_plotly_plot("classification_match_against_target_drawing")
classification_match_against_model = read_plotly_plot("classification_match_against_target_interpolation")

classification_section_match_against_logs = read_plotly_plot("classification_section_match_against_target_log")
classification_section_match_against_drawing = read_plotly_plot("classification_section_match_against_target_drawing")
classification_section_match_against_model = read_plotly_plot("classification_section_match_against_target_interpolation")

summary_against_logs = read_plotly_plot("summary_against_target_log")
summary_against_drawing = read_plotly_plot("summary_against_target_drawing")
summary_against_model = read_plotly_plot("summary_against_target_interpolation")

#statistics_against_logs = read_plotly_plot("statistics_against_target_log")
#statistics_against_drawing = read_plotly_plot("statistics_against_target_drawing")
#statistics_against_model = read_plotly_plot("statistics_against_target_interpolation")

tab1_content = dbc.Tabs([
    dbc.Tab(dcc.Graph(figure=classification_match_against_logs), label='Matches'),
    dbc.Tab([
        dcc.Graph(figure=summary_against_logs, style={'height': '230px'}),
        dcc.Graph(figure=classification_section_match_against_logs, style={'height': '280px'}),
    ], label='Detailed Outcomes'),
    #dbc.Tab(dcc.Graph(figure=classification_outcome_against_logs), label='Example Rook'),
    dbc.Tab(dcc.Graph(figure=classification_outcome_against_logs), label='Batch Level Analysis'),
])

tab2_content = dbc.Tabs([
    dbc.Tab(dcc.Graph(figure=classification_match_against_drawing), label='Matches'),
    dbc.Tab([
        dcc.Graph(figure=summary_against_drawing, style={'height': '230px'}),
        dcc.Graph(figure=classification_section_match_against_drawing, style={'height': '280px'}),
    ], label='Detailed Outcomes'),
    #dbc.Tab(dcc.Graph(figure=classification_outcome_against_drawing), label='Example Rook'),
    dbc.Tab(dcc.Graph(figure=classification_outcome_against_drawing), label='Batch Level Analysis'),
])

tab3_content = dbc.Tabs([
    dbc.Tab(dcc.Graph(figure=classification_match_against_model), label='Matches'),
    dbc.Tab([
        dcc.Graph(figure=summary_against_model, style={'height': '230px'}),
        dcc.Graph(figure=classification_section_match_against_model, style={'height': '280px'}),
    ], label='Detailed Outcomes'),
    #dbc.Tab(dcc.Graph(figure=classification_outcome_against_model), label='Example Rook'),
    dbc.Tab(dcc.Graph(figure=classification_outcome_against_model), label='Batch Level Analysis'),
])

user_content = html.Div(
    [
        #html.Div([
        #    html.Strong("Blabla...")
        #], style=dict(textAlign='center')),
        dbc.Row([
            dbc.Col([
                dcc.Graph(figure=microvu_reference_classification, style={'height': '350px'}),
                dcc.Graph(figure=fig, style={'height': '230px'})
            ], width=6),
            dbc.Col([
                dbc.Tabs(
                    [
                        dbc.Tab(tab1_content, label="Logs"),
                        dbc.Tab(tab2_content, label="Drawing"),
                        dbc.Tab(tab3_content, label="Model"),
                        dbc.Tab(dcc.Graph(figure=value_comparison), label="Value Comparison - MicroVu/Keyence")
                    ]
                )      
            ], width=6)
        ])
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

