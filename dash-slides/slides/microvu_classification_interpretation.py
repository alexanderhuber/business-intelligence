###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "POI Classification - Interpretation"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

#fig = read_plotly_plot('Input_data')

user_content = html.Div(
    [
        dbc.Row([
            dbc.Col([
                html.Div([
                    html.H5("Findings"),
                    html.P("Placeholder for Text: Offset between Keyence and MicroVu visible. Possible need for rechecking MicroVu target values -> different values in different specifications. Once we use our build model as reference, we end up getting good results for batches 10 and 11.")
                ])
            ]),
            dbc.Col([
                html.Div([
                    html.H5("Limitations"),
                    html.P("Placeholder for Text: Alignment may influence accuracy. Distances not checked!")
                ])
            ])
        ])
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

