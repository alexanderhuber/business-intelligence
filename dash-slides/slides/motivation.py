###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Problem Statement & Motivation"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

#fig = read_plotly_plot('rook_model_3d')

user_content = html.Div(
    [
        html.Div([
            dbc.Card(
                dbc.CardBody(
                    [
                        html.H5("Motivation", className="card-title"),
                        html.P("The physical measurement (MicroVu) has a high-accuracy, but is time-consuming and costly:"),
                        html.Ul([
                            html.Li("Time Consuming"),
                            html.Ul([
                                html.Li("Manual task"),
                                html.Li("Additional preparation needed")
                            ]),
                            html.Li("Result availabilty"),
                            html.Ul([
                                html.Li("No direct feedback-loop possible")
                            ])
                        ]),
                        html.Br(),
                        html.P(children=[
                            html.Strong("Intuition: "),
                            "Quality statements based on Keyence data would allow to control and manage production cycle in ad-hoc fashion"
                        ]),
                        html.Br(),
                        html.Br(),
                        html.H5("Benefits"),
                        html.Ul([
                            html.Li("More interaction possibilieties due to measurement results"),
                            html.Li("Cost savings")
                        ])
                    ])
            )
        ])
    ])
        
content = html.Div(
    [
        header,
        user_content
    ]
)

