###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

import pandas as pd

#df = pd.read_csv('assets/feature_values.csv', index_col=0)
#table = dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True)

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Classification Based On Points Of Interest (POI)"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center', color='green')),
        html.Hr()
    ])

table = read_plotly_plot('microvu_targets_table')
fig = read_plotly_plot('comparison_microvu_target_values')

user_content = html.Div(
    [
        html.Div([
            html.H5("Target Values"),
            html.Strong("Varying target/desired values defined in different locations")
        ], style=dict(textAlign='center')),
        dbc.Tabs(
            [
                dbc.Tab([
                    dcc.Graph(figure=table),
                    #table,
                    html.Div([
                        dbc.Button("Show drawing specification", id="open-xl", color="primary")
                    ], style=dict(textAlign='center')),
                    dbc.Modal(
                        [
                            dbc.ModalHeader("Engineering Drawing - Value Specification"),
                            dbc.ModalBody(html.Img(src="assets/measuring_points_2.PNG")),
                            dbc.ModalFooter(
                                dbc.Button("Close", id="close-xl", className="ml-auto", color="primary")
                            ),
                        ],
                        id="modal-xl",
                        size="lg",
                    )
                ], label="Values"),
                dbc.Tab(dcc.Graph(figure=fig), label="Comparison")
            ]
        )        
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

def toggle_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


app.callback(
    Output("modal-xl", "is_open"),
    [Input("open-xl", "n_clicks"), Input("close-xl", "n_clicks")],
    [State("modal-xl", "is_open")],
)(toggle_modal)