###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Conclusion & Considerations"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

#fig = read_plotly_plot('Input_data')

user_content = html.Div(
    [
        dbc.Row([
            dbc.Col([
        dbc.Card([
            dbc.CardBody(
                [
                    html.H5("Recalling Analysis Questions...", className="card-title"),
                    dbc.Card([
                        dbc.CardBody([
                            html.H5("Data Quality"),
                            html.Ul(html.Li("What information can be conveyed from the Keyence data?")),
                            html.P("Lots of data preparation needed", style=dict(color='green')),
                            html.P("Initial struggle of data availability - Enhanced over time", style=dict(color='green')),
                            html.P("Only diameter information directly available - Lengths not covered", style=dict(color='green'))
                        ])
                    ]),
                    html.Br(),
                    dbc.Card([
                        dbc.CardBody([
                            html.H5("Classification Possibilities"),
                            html.Ul([
                                html.Li("Is it possible to classify the produced parts as it is done with the MicroVu system?"),
                                html.Ul(html.Li("Do the Keyence data permit dropping the MicroVu system?"))
                            ]),
                            html.P("MicroVu measurements can only be replicated partially - Lengths not covered", style=dict(color='green')),
                            html.P("For now the Keyence measurement cannot serve as direct replacement for the MicroVu system", style=dict(color='green'))
                        ])
                    ])
                ]
            )
        ])
            ], width=7),
            dbc.Col([
                html.Br(),
                html.Br(),
                html.Br(),
                html.Br(),
                html.Br(),
                dbc.Card([
            dbc.CardBody([
                            html.H5("Considerations", className='card-title'),
                            html.P("We see big potential for further analysis with the Keyence data, but...", style=dict(color='green')),
                            html.P("...further efforts would be needed to fully exploit the potential", style=dict(color='green')),
                            html.Ul([
                                html.Li("Cover distances -> Marker or additional measurement in lying position"),
                                html.Li("Better preparation of parts to be measured -> Cleaning, Removal of chips...")
                            ])
                        ])
        ]
        )
            ])
        ])       
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

