###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Data Exploration"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

rooks_per_batch = read_plotly_plot('rooks_per_batch')
points_per_rook = read_plotly_plot('data_points_per_rook')
reference_table = read_plotly_plot('microvu_reference_classification_table')
measurement_per_batch = read_plotly_plot('measurement_duration_per_batch')

user_content = html.Div(
    [
        #html.H5("First Step: Explore the given data!"),
        dbc.Row([
            dbc.Col([
                dcc.Graph(figure=rooks_per_batch)
            ]),
            dbc.Col([
                dcc.Graph(figure=points_per_rook)
            ]),
            dbc.Col([
                dcc.Graph(figure=measurement_per_batch)
            ])
        ]),
        dcc.Graph(figure=reference_table)
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

