###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Classification Based On Points Of Interest (POI)"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center', color='green')),
        html.Hr()
    ])

fig = read_plotly_plot('diam_variation_across_batches')

user_content = html.Div(
    [
        html.Div([
            html.H5("Value Deviations on POIs"),
        ], style=dict(textAlign='center')),
        html.Div(dcc.Graph(figure=fig)),
        html.Div([
            html.H5("Possible Application Scenario:"),
            html.P([
                html.Strong("Direct feedback loop - ad-hoc: "),
                "Rolling mean to quickly detect overall deviations in order to possible change relevant machining parameters"
            ])
        ], style=dict(textAlign="center"))
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

