###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Problem Statement & Motivation"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

#fig = read_plotly_plot('Input_data')

user_content = html.Div(
    [
        dbc.Card([
            dbc.CardBody(
                [
                    html.H5("Analysis Questions", className="card-title"),
                    dbc.Card([
                        dbc.CardBody([
                            html.H5("Data Quality"),
                            html.Ul(html.Li("What information can be conveyed from the Keyence data?"))
                        ])
                    ]),
                    html.Br(),
                    dbc.Card([
                        dbc.CardBody([
                            html.H5("Classification Possibilities"),
                            html.Ul([
                                html.Li("Is it possible to classify the produced parts as it is done with the MicroVu system?"),
                                html.Ul(html.Li("Do the Keyence data permit dropping the MicroVu system?"))
                            ])
                            
                        ])
                    ])
                ]
            )
        ])
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

