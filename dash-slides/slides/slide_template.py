###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Contents"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

#fig = read_plotly_plot('Input_data')

user_content = html.Div(
    [
        dcc.Markdown(
            """
            1. Motivation
            2. Data Overview
                2.1 Data Transformation
            3. Findings
                3.1 Limitations
            4. Conclusion
            """
        )
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

