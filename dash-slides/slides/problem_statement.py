###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Problem Statement & Motivation"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

#fig = read_plotly_plot('rook_model_3d')

user_content = html.Div(
    [
        html.Div([
            dbc.Card(
                dbc.CardBody(
                    [
                        dbc.Row([
                            dbc.Col([
                                html.H5("Objective", className="card-title"),
                                html.P("A particular component must be produced according to defined standards and tolerances"),
                                html.Br(),
                                html.Br(),
                                html.H5("Verification of Production Quality"),
                                html.Ul([
                                    html.Li("Keyence"),
                                    html.Ul(html.Li("Optical measurement")),
                                    html.Li("MicroVu"),
                                    html.Ul(html.Li("Physical measurement"))
                                ])
                            ]),
                            dbc.Col(html.Img(src="assets/engineering_drawing.PNG", height='270px'))
                        ])                       
                    ]
                )
            ),
            html.Br(),
            dbc.Card(
                dbc.CardBody([
                 html.H5("Situation"),
                 dbc.Row([
                        dbc.Col([
                            html.P("Keyence"),
                            html.Ul([
                                html.Li("Performed during the production cycle"),
                                html.Ul(html.Li("Gathered data not used for verifying produced parts yet"))
                            ])
                        ]),
                        dbc.Col([
                            html.P("MicroVu"),
                            html.Ul([
                                html.Li("Only performed after production cycle is finished"),
                                html.Ul(html.Li("Done by external partner"))
                            ])
                        ])
                    ])   
                ])
            )
        ])
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

