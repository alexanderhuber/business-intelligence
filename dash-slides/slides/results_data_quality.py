###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Data Quality"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

fig_quality_robot = read_plotly_plot('data_quality_robot')
fig_quality_elevator = read_plotly_plot('data_quality_elevator')


tab2_content = html.Div([
    dcc.Graph(figure=fig_quality_elevator),
    dbc.Row([
        dbc.Col([html.Div([
            html.H5("Data Availability:"),
            dbc.Card("Complete contour", color="success", outline=True)
        ])]),
        dbc.Col([html.Div([
            html.H5("Data Accuracy:"),
            dbc.Card("Anomaly count: 8 (~ 16%)", color="danger", outline=True)
        ])])
    ]
    )
])

tab1_content = html.Div([
    dcc.Graph(figure=fig_quality_robot),
    dbc.Row([
        dbc.Col([html.Div([
            html.H5("Data Availability:"),
            dbc.Card("Only partial contour", color="danger", outline=True)
        ])]),
        dbc.Col([html.Div([
            html.H5("Data Accuracy:"),
            dbc.Card("Anomaly count: 1 (~ 2%)", color="success", outline=True)
        ])])
    ]
    )
])

user_content = html.Div(
    [
        html.Div([
            html.Strong("Clear reflection of different variants used for recording the measurement data!")
        ], style=dict(textAlign='center')),
        dbc.Row([
                dbc.Col(tab1_content),
                dbc.Col(tab2_content)
            ])
    ]
)

content = html.Div(
    [
        header,
        user_content
    ]
)

