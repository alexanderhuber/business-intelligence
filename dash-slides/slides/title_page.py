###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State



content = dbc.Jumbotron(children=[
            html.H1("Quality Prediction Solely From Keyence Data", className="display-3"),
            html.P(
                "VU Business Intelligence II - WS 2020", className="lead",
            ),
            html.Hr(className="my-2"),
            html.P([
                html.Small('Amanda de Paula Cristino Hirschl'),
                html.Sup('1'),
                html.Small(' & '),
                html.Small('Alexander Huber'),
                html.Sup('2')
            ]),
            html.P([
                html.Sup('1'),
                html.Small('a11937942@unet.univie.ac.at'),
                html.Br(),
                html.Sup('2'),
                html.Small('alexander.huber@univie.ac.at'),
            ])
    ], style=dict(position='fixed', left='50%', top='50%', transform='translate(-50%, -50%)', width='75%')
)