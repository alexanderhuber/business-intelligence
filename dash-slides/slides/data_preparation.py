###
# no need to delete this - it won't show up in the presentation unless you add it to presentation.py
###

# necessary imports - do not change
from app import app

# custom imports - delete these if you don't need them
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State

from custom_utilities.custom_functions import read_plotly_plot

header_text = "Data Preparation & Anomaly Detection"
header = html.Div([
        html.H2(header_text, style=dict(textAlign='center')),
        html.Hr()
    ])

#fig = read_plotly_plot('Input_data')

user_content = html.Div(
    [
        html.Img(src="assets/data_preparation.png", width='900px'),
        html.Br(),
        html.Br(),
        html.Br(),
        html.Img(src="assets/before_after.png", width='500px')
    ], style=dict(textAlign='center')
)

content = html.Div(
    [
        header,
        user_content
    ]
)

