###
# EDIT THIS PAGE BUT DO NOT REMOVE THE OBJECTS
###

# the order of the slides, using the names of the files
# NOTE - slide filenames MUST BE VALID PYTHON VARIABLE NAMES
slide_order = [
    'title_page',
    #'toc',
    'motivation_title',
    'problem_statement',
    'motivation',
    'analysis_questions',
    'preparation_title',
    'data_exploration',
    'data_preparation',
    'results_data_quality',
    'classification_title',
    'classification_tasks',
    'microvu_targets_dilemma',
    'microvu_classification_results',
    'diam_variations',
    'classification_overall_contour',
    #'microvu_classification_interpretation',
    #'limitations_fw_title',
    #'limitations_future_work',
    'conclusion_title',
    'conclusion',
    'end_page'
]

# the text that appears in the tab
presentation_title = 'Dash Slides'

# the text that appears in the previous and next buttons
prev_text = '< '
next_text = ' >'