import pandas as pd
import numpy as np
from scipy.signal import argrelextrema
import warnings


from scipy.signal import wiener, filtfilt, butter, gaussian, freqz
from scipy.ndimage import filters
from statsmodels.tsa.api import ExponentialSmoothing, SimpleExpSmoothing


class DataPreprocessor:

    #def __init__(self):
        #self.raw = data
        #self.data = data[data['value'] < 999]
        
    
    def resample(self, data):
        data = data[data['seconds_scaled'].between(0, 50, inclusive=True)]
        resampled_data = data.groupby('name', sort=False).apply(self.__resample_instance).drop('name', axis=1).reset_index().drop('level_1', axis=1)
        resampled_data['position'] = resampled_data['timestamp'].dt.total_seconds() #* 10
        resampled_data.drop('timestamp', axis=1, inplace=True)
        resampled_data = resampled_data.astype({'name':'int', 'batch':'int'})
        #data.loc[data['name'] == 2939, 'value'] = data[data['name'] == 2939]['value'].iloc[::-1].values
        resampled_data = resampled_data.groupby('name', sort=False).apply(self.__remove_initial_spike).reset_index()
        section_info = pd.read_csv('sections.csv', index_col=0)
        get_section_info = lambda x: section_info[(section_info['min'] <= x['position']) & (section_info['max'] >= x['position'])]['section'].values[0]
        resampled_data['section'] = resampled_data.apply(get_section_info, axis=1)
        return resampled_data
    
    
    def __resample_instance(self, data):
        data = data.drop_duplicates(subset='seconds_scaled')
        min_position = data[data.seconds_scaled == data.seconds_scaled.min()].copy()
        min_position['seconds_scaled'] = 0
        max_position = data[data.seconds_scaled == data.seconds_scaled.max()].copy()
        max_position['seconds_scaled'] = 50
        data = data.append([min_position, max_position])
        data['timestamp'] = pd.to_timedelta(data.seconds_scaled, unit='S')
        cols = list(data.columns)
        agg_dict = {x: 'first' if x != 'value' else 'mean' for x in cols}
        resampled_data = data.groupby(pd.Grouper(key='timestamp', freq='50ms')).agg(agg_dict)
        index = resampled_data.index.name
        resampled_data = resampled_data.drop(index, axis=1)
        resampled_data = resampled_data.pad().reset_index()
        return resampled_data
    
    
    def __remove_initial_spike(self, df):
        ## Removal of initial spikes - seconds set manually here: Trial end error
        first_index = df.index[0]
        try:
            first_good_index = df.loc[(df['value'] < 22.1) & (df['value'] > 21.9)].index[0]
            first_good_value = df.at[first_good_index, 'value']
            df.at[first_index:first_good_index, 'value'] = first_good_value
        except:
            pass
        return df
        
    ###### Old methods
        
    def get_processed_data(self, time_span=100, sample_freq='100L'):
        # Filter out ledges
        data = self.data[self.data['value'] < 999]
        # Normalize scannings
        data = self.normalize_scan_times(data, time_span, scale_times=True)
        # Resample data
        #instance_data = data.groupby('name', sort=False)
        #resampled_data = instance_data.apply(lambda x: self.resample(x, sample_freq))
        #data = resampled_data.drop('name', axis=1).reset_index()
        # Fix reversed series
        #data = self.__correct_reversed_series(data)
        # Remove initial spikes for batches 10 and 11
        #data = self.__remove_initial_spikes(data, sample_freq)
        # Smooth steep steps
        #data = self.__smooth_steep_diam_changes(data, 1.35, 6, time_span, sample_freq)
        # General smoothing
        #data = self.__smooth_series(data)
        return data

    def set_batches(self, batches):
        self.data = self.data[self.data['batch'].isin(batches)]

    def __get_correlations(self, data, reverse=False):
        instance_data = data.groupby('name', sort=False)['value']
        instance_data = instance_data.apply(lambda x: pd.Series(x.values)).unstack()
        # Get median from observations
        median = instance_data.median(axis=0)
        if reverse:
            # Reverse measurement data
            instance_data = instance_data.apply(self.__reverse_row_values, axis=1)
        # Get correlations
        corr = instance_data.corrwith(median, axis=1)
        return pd.Series(corr, index=instance_data.index)

    def __correct_reversed_series(self, data):
        corr = self.__get_correlations(data)
        corr_reversed = self.get_correlations(data, reverse=True)
        df = pd.concat([corr, corr_reversed], axis=1)
        # Define wrong series based on correlation values / parameters probably need to be tuned 
        no_good_orig_correlation = df[0] < 0.9
        better_reversed_correlation = df[1] > 0.9
        series_to_reverse = df.where(no_good_orig_correlation & better_reversed_correlation).dropna()
        names = series_to_reverse.index.to_list()
        warnings.warn(f"The following series got reversed, as I assumed it was reversed perhaps: {str(names)}")
        instance_data = data.groupby('name', sort=False)
        return instance_data.apply(lambda x: self.__reorder(x, names))

    def __reverse_row_values(self, x):
        idx = x.index
        return pd.Series(x.iloc[::-1].values, index=idx)

    def __reorder(self, x, names):
        if x.name in names:
            x['value'] = x['value'].iloc[::-1].values
        return x

    def __remove_initial_spikes(self, df, sample_freq):
        instance_data = df.groupby('name', sort=False)
        df = instance_data.apply(self.__remove_spike_from_single_series).drop(['name'], axis=1).reset_index()
        df = df.drop('level_1', axis=1)
        df = self.normalize_scan_times(df, 100)
        # Resample data
        instance_data = df.groupby('name', sort=False)
        resampled_data = instance_data.apply(lambda x: self.resample(x, sample_freq))
        df = resampled_data.drop('name', axis=1).reset_index()
        return df
    
    def __remove_spike_from_single_series(self, df):
        ## Removal of initial spikes - seconds set manually here: Trial end error
        df = df.where((df['batch'] < 10) | (df['timestamp'].dt.total_seconds() > 3)).dropna()
        #df = df.where((df['batch'] > 9) | (df['timestamp'].dt.total_seconds() > 2)).dropna()
        first_index = df.index[0]
        first_good_index = df.loc[df['value'] < 22.1].index[0]
        first_good_value = df.at[first_good_index, 'value']
        df.at[first_index:first_good_index, 'value'] = first_good_value
        return df
        