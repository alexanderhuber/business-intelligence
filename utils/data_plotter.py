import altair as alt
alt.data_transformers.disable_max_rows()

import plotly.io as pio
import plotly.express as px
import plotly.graph_objs as go

pio.templates["custom"] = pio.templates["gridon"]

pio.templates["custom"].update({
'layout.font.family': 'Montserrat',
'layout.colorway': px.colors.qualitative.Bold
#e.g. you want to change the background to transparent
#'paper_bgcolor': 'rgba(0,0,0,0)',
#'plot_bgcolor': 'rgba(0,0,0,0)'
})

pio.templates.default = "custom"


class DataPlotter:

    # Common plot method
    def plot_facet(self, df, x, y, group_by, n_cols=3, title="", subtitle="", plot_width=600, plot_height=300):
        data = df.copy()
        data = self.get_altair_df(data)
        #x_format = ' [s]' if x == 'timestamp' else ''
        #y_format = ' [mm]' if y == 'value' else ''
        plot = alt.Chart(data).mark_line().encode(
            x=alt.X(x, axis=alt.Axis(title=x.capitalize())),
            y=alt.Y(y, axis=alt.Axis(title=y.capitalize())),
            facet=alt.Facet(group_by + ':N', columns=n_cols)
        ).properties(
            title={
              "text": "Input data", 
              "subtitle": [title, subtitle],
            },
            width=plot_width,
            height=plot_height
        ).configure(background='transparent').configure_title(
            fontSize=20,
            align='center',
            anchor='middle'
        )
        return plot

    def plot_hist(self, data, bin_variable, plot_width=300, plot_height=150):
        plot = alt.Chart(data).mark_bar().encode(
            x=alt.X(bin_variable, bin=True),
            y='count()'
        ).properties(
            width=plot_width,
            height=plot_height
        )
        return plot

    # Helper method to change timedelta columns to total seconds (float), as altair can't deal with timedeltas.
    def get_altair_df(self, df):
        td_cols = list(df.select_dtypes(include='timedelta64').columns)
        for col in td_cols:
            df[col] = df[col].dt.total_seconds()
        df = df.astype({'batch': str})
        return df
    
    
    # Plotly
    def save_plotly_plot(self, fig, file):
        plot_path = 'dash-slides/data/plots'
        pio.write_json(fig, f"{plot_path}/{file}.json")
    
    def read_plotly_plot(self, file):
        plot_path = 'dash-slides/data/plots'
        return pio.read_json(f"{plot_path}/{file}.json")
    
    def plot(self, kind, data, **kwargs):
        if kind == 'line':
            fig = px.line(data, **kwargs)
        if kind == 'scatter':
            fig = px.scatter(data, **kwargs)
        if kind in ['boxplot', 'box']:
            fig = px.box(data, **kwargs)
        if kind == 'sunburst':
            fig = px.sunburst(data, **kwargs)
        if kind == 'bar':
            fig = px.bar(data, **kwargs)
        if kind == 'imshow':
            fig = px.imshow(data, **kwargs)
        #fig.update_xaxes(showline=True, mirror=True)
        #fig.update_yaxes(showline=True, mirror=True)
        fig.update_xaxes(showline=True, mirror=True, ticks='outside', zeroline=False)
        fig.update_yaxes(showline=True, mirror=True, ticks='outside', zeroline=False)
        return fig