from database.data_importer import DataManagement
import pandas as pd
pd.options.mode.chained_assignment = None
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans, DBSCAN
import numpy as np
import seaborn as sn
import altair as alt
from utils.data_preprocessor import DataPreprocessor
from utils.data_plotter import DataPlotter

import numpy as np
import pandas as pd
import altair as alt
from altair import datum
import qgrid

# Options
pd.options.mode.chained_assignment = None


# manage dfs for plots
class BatchComparison:

    def __init__(self):
        self.df, self.durations, self.measurements = self.prepare_data()

    def prepare_data(self):
        dm = DataManagement()
        df = dm.db_query('select * from working_data')
        df = df.query("type =='measurement'")

        durations = df.groupby(['batch', 'name'])[['timestamp']].min()
        durations['start'] = pd.to_datetime(df.groupby(['batch', 'name'])[['timestamp']].min()['timestamp'].str.strip(),
                                            utc=True)
        durations['end'] = pd.to_datetime(df.groupby(['batch', 'name'])[['timestamp']].max()['timestamp'].str.strip(),
                                          utc=True)
        durations['dur'] = durations['end'] - durations['start']
        durations['seconds'] = durations['dur'] / np.timedelta64(1, 's')
        durations = durations.reset_index()

        measurements = df.groupby(['batch', 'name'])[['value']].min()
        measurements = measurements.reset_index()

        return df, durations, measurements

    def get_quantities(self):
        df = self.df
        batches = np.unique(df[['batch']].to_numpy())
        batches_info = pd.DataFrame()
        rooks = []
        for batch in batches:
            # batch = str(batch)
            df_batch = df.query("batch == @batch")
            df_batch
            m_count = df_batch.groupby('name', as_index=False)[['value']].count()
            rooks.append(len(df_batch.groupby('name')))

        batches_info['batch'] = batches
        batches_info['rook'] = rooks

        # batches_info[['batch', 'rook']]
        bars = alt.Chart(batches_info).mark_bar().encode(
            alt.X('rook:Q', title='Quantity'),
            alt.Y('batch:O', title='Batch')
        ).properties(
            height=150,
            width=700,
            title='Quantity of parts per batch'
        )

        text = bars.mark_text(
            align='left',
            baseline='middle',
            dx=3
        ).encode(
            text='rook:Q'
        ).properties(
            height=150,
            width=700,
            title='Quantity of parts per batch'
        )

        quantity = (bars + text)
        quantity.properties(
            height=150,
            width=700,
            title='Quantity of parts per batch'
        ).configure_axis(
            labelFontSize=12,
            titleFontSize=12
        ).configure_title(
            fontSize=14
        )
        quantity
        return quantity


    def get_measurements_duration(self):
        durations = self.durations
        source = durations[['batch', 'seconds']]

        error_bars = alt.Chart(source).mark_errorbar(extent='stdev').encode(
            x=alt.X('seconds:Q', scale=alt.Scale(zero=False)),
            y=alt.Y('batch:O')
        ).properties(
            height=150,
            width=700,
            title='Measurements duration with standart deviation interval'
        )

        points = alt.Chart(source).mark_point(filled=True, color='black').encode(
            x=alt.X('seconds:Q', aggregate='mean'),
            y=alt.Y('batch:O'),
        ).properties(
            height=150,
            width=700,
            title='Measurements duration with standart deviation interval'
        )

        duration = (error_bars + points)
        duration.properties(
            height=150,
            width=700,
            title='Measurements duration with standart deviation interval'
        ).configure_axis(
            labelFontSize=12,
            titleFontSize=12
        ).configure_title(
            fontSize=14
        )

        return duration


    def get_measurements_values(self):
        source = self.df.query('value<"999"')[['batch', 'value']]
        plot = alt.Chart(source).mark_boxplot().encode(
            x='batch:O',
            y='value:Q'
        ).properties(
            height=350,
            width=700,
            title='Measurements values with min and max whiskers'
        ).configure_axis(
            labelFontSize=12,
            titleFontSize=12
        ).configure_title(
            fontSize=14
        )

        return plot