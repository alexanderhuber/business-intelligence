from collections import Counter
import yaml #pip install pyyaml
import os

class Log:

    def __init__(self):
        self.log_header = {}
        self.log_body = {}
        self.logs = {}
        self.splitter = "---"

    def get_elements(self, file, returnSeparate=False, getHeaderOnly=False):
        header = []
        body = []

        with open(file, 'r') as stream:
            data = stream.read()
            data = data.split(self.splitter)

            for event in data:
                try:
                    if event != "" and event is not None:
                        if 'log' in event:
                            header.append(yaml.safe_load(event))
                            if getHeaderOnly:
                                return header[0]
                        else:
                            body.append(yaml.safe_load(event))
                except yaml.YAMLError as exc:
                    print(exc)

        self.log_header[file] = header
        self.log_body[file] = body

        if returnSeparate:
            return header, body

        self.logs[file] = header + body
        return self.logs[file]

    def get_unique_attr(self, file, attr):
        unique_attr = set()

        if file not in self.logs:
            self.get_elements(file, returnSeparate=False)

        for e in self.logs[file]:
            if 'event' in e:
                if attr in e['event']:
                    unique_attr.add(e['event'][attr])
        return unique_attr

    def count_events_per_attr(self, file, attr):
        unique_attr = []

        if file not in self.logs:
            self.get_elements(file)

        for e in self.logs[file]:
            if 'event' in e:
                if attr in e['event']:
                    unique_attr.append(e['event'][attr])
        return Counter(unique_attr)


    def print_probable_order(self, file):
        order = []

        if file not in self.logs:
            self.get_elements(file)

        for e in self.logs[file]:
            if 'event' in e:
                name = e['event']['concept:name']

                if e['event']['lifecycle:transition'] == "start" or name not in order:
                    if name not in order:
                        order.append(name)
        for i in order:
            print(i)

    def get_files_with_value(self, directory, dict_path, value, header_only=True):
        files_count = len(os.listdir(directory))
        curr_count = 0
        files_list = []

        for filename in os.listdir(directory):
            path = os.path.join(directory, filename)
            if header_only:
                d = self.get_elements(path, getHeaderOnly=True)
            else:
                d = self.get_elements(path)

            try:
                for k in dict_path:
                    d = d[k]

                if d in value:
                    files_list.append(path)

            except KeyError:
                pass

        return files_list

"""
log_reader = Log()

# Getting all files in which the target appears in the specified keys
folder = "data/batch07_bus/"
keys = ['log', 'trace', 'cpee:name']
target = ["Turm Keyence Measurement"]
files = log_reader.get_files_with_value(directory=folder, dict_path=keys, value=target, header_only=True)
print(files)

"""
"""
for file in files:
    path = folder + file
    counter = log_reader.count_events_per_attr(path, "cpee:lifecycle:transition")
    print(file, ": ", counter['activity/receiving'])


file = "data/batch07_bus/0bb8ceb8-688c-4eac-b18a-8db7c92cb577.xes.yaml"

print("Unique concept names: ")
print(log_reader.get_unique_attr(file, "concept:name"))

print("_______________________")
print("Unique instances: ")
print(log_reader.get_unique_attr(file, "cpee:instance"))

print("_______________________")
print("Unique activity uuids: ")
print(log_reader.get_unique_attr(file, "cpee:activity_uuid"))

print("_______________________")
print("Unique lifecycle transitions: ")
print(log_reader.get_unique_attr(file, "lifecycle:transition"))

print("_______________________")
print("Unique cpee lifecycle transitions: ")
print(log_reader.get_unique_attr(file, "cpee:lifecycle:transition"))

print("_______________________")
print("Event order by timestamp:")
log_reader.print_probable_order(file)

print("_______________________")
print("Count events per concepts: ")
print(log_reader.count_events_per_attr(file, "concept:name"))
"""