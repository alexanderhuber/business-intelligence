from data_importer import DataManagement
import os

dm = DataManagement()

# build relative paths
batch07 = os.path.join(os.path.dirname(os.getcwd()), 'data/batch07_bus/')
batch08 = os.path.join(os.path.dirname(os.getcwd()), 'data/batch08_bus/')
batch09 = os.path.join(os.path.dirname(os.getcwd()), 'data/batch09_bus/')
batch10 = os.path.join(os.path.dirname(os.getcwd()), 'data/batch10_bus/')
batch11 = os.path.join(os.path.dirname(os.getcwd()), 'data/batch11')

#dm.import_data(batch11, 11)

df = dm.db_query('select * from working_data')
print(df.groupby('batch').first())